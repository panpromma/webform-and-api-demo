﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestCreateAPI.Models
{
    public class PurchasingModel
    {
        [Editable(false)]
        [Display(Name = "เลขที่เอกสาร")]
        public string DocNo { get; set; }

        [Editable(true)]
        [Display(Name = "สถานะงาน")]
        public string DocStatus { get; set; }

        [Display(Name = "รหัสพนักงานผู้ขอ")]
        public string RequestUserNo { get; set; }

        [Display(Name = "ชื่อพนักงานผู้ขอ")]
        public string RequestUserName { get; set; }

        [Display(Name = "ตำแหน่งงานผู้ขอ")]
        public string RequestJob { get; set; }

        [Display(Name = "หน่วยงานผู้ขอ")]
        public string RequestAgency { get; set; }

        [Display(Name = "ประเภทการจัดซื้อจัดจ้าง")]
        public string ProcurementTypeCode { get; set; }
        public List<SelectListItem> ProcurementTypes { get; set; }

        [Display(Name = "ประเภทงาน")]
        public string TypeCode { get; set; }
        public List<SelectListItem> Types { get; set; }

        [Display(Name = "ชื่อการจัดซื้อหรือจัดจ้าง")]
        public string ProcurementName { get; set; }

        [Display(Name = "เหตุผลและความจำป็น")]
        public string Reason { get; set; }

        [Display(Name = "จำนวน")]
        public string Qty { get; set; }

        [Display(Name = "วงเงินงบประมาณ")]
        public string BudgetLimit { get; set; }

        [Display(Name = "จำนวนเงินรวมตามรายการขอซื้อหรือจ้าง(บาท)")]
        public string BudgetTotal { get; set; }

        [Display(Name = "กำหนดเวลาที่ต้องการได้รับพัสดุ")]
        public string ReceiveTime { get; set; }

        [Display(Name = "วิธีที่จะซื้อหรือจ้าง")]
        public string HowTo { get; set; }

        [Display(Name = "ตามมาตรา")]
        public string Section { get; set; }

        [Display(Name = "เหตุผลของการเลือกวิธี")]
        public string ReasonsForChoosing { get; set; }

        [Display(Name = "ประธานกรรมการ")]
        public int ChairmanBoardReview { get; set; }

        [Display(Name = "ประเภท")]
        public int AuditType { get; set; }

        [Display(Name = "ประธานกรรมการ")]
        public int ChairmanBoardAudit { get; set; }

    }
}
