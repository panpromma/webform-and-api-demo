﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCreateAPI.Models
{
    public class InputAppointmentModel
    {
        public DateTime DateTime { get; set; }
        public long TreatmentId { get; set; }
        public long AppointmentId { get; set; }
        public long AppointType { get; set; }
    }
}
