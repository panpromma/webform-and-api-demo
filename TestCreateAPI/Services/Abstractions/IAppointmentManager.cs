﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCreateAPI.Controllers;
using TestCreateAPI.Models;

namespace TestCreateAPI.Services.Abstractions
{
    public  interface IAppointmentManager
    {
        Task<output> AddAppointMent(InputAppointmentModel input);

        Task<bool> DeleteJob(string jobId);
    }
}
