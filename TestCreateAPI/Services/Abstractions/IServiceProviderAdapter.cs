﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCreateAPI.Models;

namespace TestCreateAPI.Services.Abstractions
{
    public   interface IServiceProviderAdapter
    {
         void ExecuteAsync(InputAppointmentModel input);

        //void Notiify(InputAppointmentModel input);
    }
}
