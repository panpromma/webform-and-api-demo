﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TestCreateAPI.Controllers;
using TestCreateAPI.Models;

namespace TestCreateAPI.Services
{
    public class Jobservice : Interface
    {
        private readonly IBackgroundJobClient _backgroundJobClient;
        public Jobservice (IBackgroundJobClient backgroundJobClient)
        {
            _backgroundJobClient = backgroundJobClient;
        }

        public async Task<output> AddJob(Input input)
        {
            output output = new output();
            string job = "";
             DateTime now = DateTime.UtcNow;
            if (input.dateT.Value.Kind != DateTimeKind.Utc) input.dateT = input.dateT.Value.ToUniversalTime();
            TimeSpan difference = input.dateT.Value.Subtract(now);
            job = _backgroundJobClient.Schedule(
                   () =>
                   this.Notiify(input),
                   difference
               );
            output.jobid = Convert.ToInt64(job);
            return output;

        } 
        

   


        public async Task<output> ForGetJobs(Input input)
        {
            output output = new output();
            string job = "";
            job = _backgroundJobClient.Enqueue(
                   () =>
                   this.Notiify(input)
               );
            output.jobid = Convert.ToInt64(job);
            return output;
        }

        public async Task<bool> DeleteJob(Input input)
        {
            string job = "";
            //DateTime now = DateTime.UtcNow;
            //if (input.dateT.Value.Kind != DateTimeKind.Utc) input.dateT = input.dateT.Value.ToUniversalTime();
            //TimeSpan difference = input.dateT.Value.Subtract(now);
            job = _backgroundJobClient.Enqueue(
                   () =>
                   this.Notiify(input)
               );
            _backgroundJobClient.Delete(job);

            return true;
        }

        public async Task<bool> Notiify(Input input)
        {
            string host = "https://notify-api.line.me/api/notify";
            string token = "RVYAocTNVI2ntBKovKay3qfp23wICOnrTbJ1LHfZKFV";
            var request = (HttpWebRequest)WebRequest.Create(host);
            string message = string.Format("message=appointment{0} เวลา{1}", input.number, input.dateT.ToString());
            var data = Encoding.UTF8.GetBytes(message);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("Authorization", string.Format("Bearer {0}", token));
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return true;
        }
    }
}
