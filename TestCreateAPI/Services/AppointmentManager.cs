﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TestCreateAPI.Controllers;
using TestCreateAPI.Models;
using TestCreateAPI.Services.Abstractions;
using TimeZoneConverter;

namespace TestCreateAPI.Services
{
    public class AppointMentManager : IAppointmentManager
    {
        private readonly IBackgroundJobClient _backgroundJobClient;
        private readonly IServiceProviderAdapter _serviceProviderAdapter;
        public AppointMentManager(IBackgroundJobClient backgroundJobClient , IServiceProviderAdapter serviceProviderAdapter)
        {
            _backgroundJobClient = backgroundJobClient;
            _serviceProviderAdapter = serviceProviderAdapter;
        }
        public async Task<output> AddAppointMent(InputAppointmentModel input)
        {
            output output = new output();
            string job = "";
            DateTime now = DateTime.UtcNow;
            if (input.DateTime.Kind != DateTimeKind.Utc) input.DateTime = input.DateTime.ToUniversalTime();
            TimeSpan difference = input.DateTime.Subtract(now);
            job = _backgroundJobClient.Schedule(
                   () =>
                     _serviceProviderAdapter.ExecuteAsync(input),
                   difference
               );
            output.jobid = Convert.ToInt64(job);
            return output;

        }
     
        public async Task<bool> DeleteJob(string jobId)
        {
            return _backgroundJobClient.Delete(jobId);

        }
    }
}
