﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCreateAPI.Controllers;
using TestCreateAPI.Models;

namespace TestCreateAPI.Services
{
 public   interface Interface
    {
        Task<output> AddJob(Input input);
        Task<output> ForGetJobs(Input input);

        Task<bool> DeleteJob(Input input);

        Task<bool> Notiify(Input input);
    }
}
