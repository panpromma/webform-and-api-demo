﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TestCreateAPI.Models;
using TestCreateAPI.Services.Abstractions;
using TimeZoneConverter;

namespace TestCreateAPI.Services
{
    public class ServiceProviderAdapter : IServiceProviderAdapter
    {
        public  void ExecuteAsync(InputAppointmentModel input)
        {
            string host = "https://notify-api.line.me/api/notify";
            string token = "RVYAocTNVI2ntBKovKay3qfp23wICOnrTbJ1LHfZKFV";
            var request = (HttpWebRequest)WebRequest.Create(host);
            string message = string.Format("message=AppointmentID {0} เวลา {1}", input.AppointmentId, Program.GenDateText(input.DateTime).ToString());
            var data = Encoding.UTF8.GetBytes(message);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("Authorization", string.Format("Bearer {0}", token));
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
    }
}
