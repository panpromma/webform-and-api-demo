﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestCreateAPI.Models;
using TestCreateAPI.Services;
using TestCreateAPI.Services.Abstractions;

namespace TestCreateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {
        private readonly IAppointmentManager _interface;
        public AppointmentController(IAppointmentManager interfaces)
        {
            _interface = interfaces;
        }
        // PUT api/<JobController>/
        [Route("api/Appoint")]
        [HttpPost]
        public async Task<ActionResult<output>> AddJob(InputAppointmentModel input)
        {
            output data = await _interface.AddAppointMent(input);

            return Ok(data);
        }

        [Route("DeleteJobAppoint")]
        [HttpPost]
        public async Task<ActionResult<bool>> DeleteJob(string jobId)
        {
            bool data = await _interface.DeleteJob(jobId);

            return Ok(data);
        }
    }
}
