﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestCreateAPI.Models;
using TestCreateAPI.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestCreateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobController : ControllerBase
    {
        private readonly Interface _interface;
        public JobController(Interface interfaces)
        {
            _interface = interfaces;
        }
        // PUT api/<JobController>/
        [Route("api/test")]
        [HttpPost]
        public  async Task<ActionResult<output>>  AddJob(Input value)
        {
            output data = await _interface.AddJob(value);

            return Ok(data);
        }

        [Route("testForGetJobs")]
        [HttpPost]
        public async Task<ActionResult<output>> ForGetJobs(Input value)
        {
            output data = await _interface.ForGetJobs(value);

            return Ok(data);
        }

        [Route("testDeleteJob")]
        [HttpPost]
        public async Task<ActionResult<bool>> DeleteJob(Input value)
        {
            bool data = await _interface.DeleteJob(value);

            return Ok(data);
        }
    }
}
