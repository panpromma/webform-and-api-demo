using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Hangfire;
using Hangfire.SqlServer;
using Hangfire.Server;
using TestCreateAPI.Services;
using TestCreateAPI.Services.Abstractions;

namespace TestCreateAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            string document = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            services.AddControllersWithViews();
            services.AddSwaggerGen(Options =>
           {
               Options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
               {
                   Title = "Place Info Service API ",
                   Version = "v1",
                   Description = "Hangfire.Sample",
               });
               Options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, document));
           });
            #region dependency injection service
            services.AddScoped<Interface, Jobservice>();
            services.AddScoped<IAppointmentManager, AppointMentManager>();
            services.AddScoped<IServiceProviderAdapter, ServiceProviderAdapter>();
            #endregion
            services.AddHangfire(configuration => configuration
            .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
            .UseSimpleAssemblyNameTypeSerializer()
            .UseRecommendedSerializerSettings());
            ///.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection"), new SqlServerStorageOptions
            //{
                //CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                //SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
               // QueuePollInterval = TimeSpan.Zero,
                //UseRecommendedIsolationLevel = true,
                //DisableGlobalLocks = true
            //}));
        }

      

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
           // app.UseHangfireServer();
            app.UseAuthorization();
           // app.UseHangfireDashboard();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwagger();
            app.UseSwaggerUI(opt => {
                opt.SwaggerEndpoint("/swagger/v1/swagger.json", "TestCreateAPI");
                opt.RoutePrefix = "document";
                });
        }
    }
}
