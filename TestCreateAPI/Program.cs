using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TimeZoneConverter;

namespace TestCreateAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
        
                });

        public static string GenDateText(DateTime date)
        {
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo("th-TH");
            var timezone = TZConvert.GetTimeZoneInfo("Asia/Bangkok");
            if (date.Kind != DateTimeKind.Utc) date = date.ToUniversalTime();
            {
                var time = TimeZoneInfo.ConvertTimeFromUtc(date, timezone);
                string xx = time.ToString("dd/MM/yyyy HH:mm", cultureInfo);
                return xx;
            }
        }
    }
}
